#!/bin/bash

if [[ -z $1 ]]; then
  echo "Usage: keyboard_backlight up/down"
  exit 1
fi

max=$(cat "/sys/class/leds/asus::kbd_backlight/max_brightness")
brightness=$(cat "/sys/class/leds/asus::kbd_backlight/brightness")

if [[ "$1" == "up" ]]; then
  if [[ "$brightness" -lt "$max" ]]; then
    echo "$((brightness+1))" >> /sys/class/leds/asus\:\:kbd_backlight/brightness
  fi
elif [[ "$1" == "down" ]]; then
  if [[ "$brightness" -gt 0 ]]; then
    echo "$((brightness-1))" >> /sys/class/leds/asus\:\:kbd_backlight/brightness
  fi
fi


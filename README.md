# dotfiles

Dotfiles for my current laptop setup, based on https://github.com/deepjyoti30/dotfiles.git (manjaro branch).

## required 
- i3-gaps -- wm
-  compton -- compositor
-  polybar -- statusbar
-  dunst -- notification
-  nitrogen -- wallpaper
-  termite -- Terminal
-  rofi -- app launcher
- i3lock-fancy
- oh-my-zsh
- Fira fonts
- Noto fonts